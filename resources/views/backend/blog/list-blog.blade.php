@extends('backend.master.master')
@section('title','List-blog')
    
@section('content')
{{--  <main class="text-gray-800 px-8 md:w-2/3 lg:w-3/5 xl:w-1/2 sm:w-full mx-auto">

    <div class="text-center">
            <h1 class="text-center text-3xl my-8">Articles</h1>
            <a class="my-3 hover:underline" href="{{ route(' blog-admin.create') }}" >New article</a>
        </div>

    @foreach ($articles as $article)
      <section class="border-b">
        <h2 class="text-3xl font-bold text-center mt-4 hover:underline"><a href="{{Route('blog-admin.show', $article->slug)}}">{{$article->title}}</a></h2>
        <p class="text-sm text-center leading-5 text-gray-700 mt-3">Posted {{\Carbon\Carbon::parse($article->updated_at)->format('d/m/Y')}}</p>
        <article class="markdown-body">
        {!! $article->summary_html!!}
          <div class="text-right mt-8">
            <a href="{{Route('blog-admin.show', $article->slug)}}" role="button" class="px-4 py-2 border border-gray-300 rounded bg-white text-sm font-medium text-gray-700 hover:border-gray-500 focus:z-10 focus:outline-none focus:border-gray-300 focus:shadow-outline-gray active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150">
              Read more
            </a>
          </div>
        </article>
      </section>
    @endforeach  --}}

    <!-- Simple Datatable start -->
    <div class="card-box mb-30">
      <div class="pd-20">
        <h4 class="text-blue h4">News Table</h4>
      </div>
      <div class="pb-20">
        <table class="data-table-1 stripe hover nowrap">
          <thead>
            <tr>
              <th class="table-plus datatable-nosort">ID</th>
              <th>Title</th>
              <th>Slug</th>
              <th>Body</th>
              <th>Date</th> 
              <th class="datatable-nosort">Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($articles as $article)
            <tr>
              <td class="table-plus">{{$article->id}}</td>
              <td>{{$article->title}}</td>
              <td>{{$article->slug}}</td>
              <td>{{$article->body_md}}</td>
              <td>{{$article->created_at}}</td>
              <td>
                <div class="dropdown">
                  <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                    <i class="dw dw-more"></i>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                    <a class="dropdown-item" ><i class="dw dw-eye"></i> View</a>
                    <a class="dropdown-item" href="Home-Admin-Shop/blog-admin/{{$article->slug}}/edit" ><i class="dw dw-edit2"></i> Edit</a>
                    <a class="dropdown-item" href="Home-Admin-Shop/blog-admin/{{$article->id}}/destroy"><i class="dw dw-delete-3"></i> Delete</a>
                    
                  </div>
                </div>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <!-- Simple Datatable End -->

@endsection