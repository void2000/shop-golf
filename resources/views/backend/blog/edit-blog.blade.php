@extends('backend.master.master')
@section('title','edit-blog')

@section('content')
<section class="w-full">

  <h3 class="text-center text-3xl font-semibold">New Article</h3>

  <form class="w-full px-6"  action="Home-Admin-Shop/blog-admin/{{$articleFound->slug}}/update" method="post" enctype="multipart/form-data">
    @csrf
    <div class="flex flex-wrap -mx-3 mb-6">
      <div class="w-full md:w-4/5 px-3 mb-6 md:mb-0">
        <label class="label" for="title">
          Title
        </label>
        <input class="input" id="title" value="{{$articleFound->title}}" name="title" style="border: 1px solid black" type="text" >
        <p class="text-red-500 text-xs italic">Please fill out this field.</p>
      </div>
      <div class="w-full md:w-1/5 px-3 mb-6 md:mb-0">
        <label class="label" for="online">
          Online?
        </label>
        <div class="relative">
          <select class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight " id="online" name="online">
            @if ($articleFound->online == 1)
                <option selected value="1">Yes</option>
                <option value="0">No</option>    
            @else
                <option  value="1">Yes</option>
                <option selected value="0">No</option>    
            @endif
          </select>
          
        </div>
      </div>
    </div>
    <div class="flex flex-wrap -mx-3 mb-6">
      <div class="w-full px-3">
        <label class="label" for="grid-password">
          Article body
        </label>
        <p class="text-gray-600 text-xs italic mb-2">This is actual article body</p>
        <textarea class="input"  name="body" id="body">{{ old('body') }}{{$articleFound->body_md}}</textarea>
      </div>
    </div>

    <div class="flex flex-wrap -mx-3 mb-6">
      <div class="w-full px-3">
        <label class="label" for="grid-password">
          Summary
        </label>
        <p class="text-gray-600 text-xs italic mb-2">This is the text that will appear in the blog index page</p>
        <textarea class="input " name="summary" id="summary">{{ old('summary') }}{{$articleFound->summary_md}}</textarea>
      </div>
    </div>

    <div class="w-full text-right pa-3 mb-6">
      <input class="btn btn-green my-4" type="submit" value="Save article">
    </div>
  </form>

</section>

  {{-- Import CSS and JS for SimpleMDE editor --}}
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
  <script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>

  <script>
        // Initialise editors
    var bodyEditor = new SimpleMDE({ element: document.getElementById("body") });
    var summaryEditor = new SimpleMDE({ element: document.getElementById("summary") });
  </script>
  <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.0.1/dist/alpine.js" defer></script>

@endsection
