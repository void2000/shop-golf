<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\model\Article;
use GitDown;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['articles'] = Article::orderByDesc('updated_at')->where('online', true)->get();
        return view('backend.blog.list-blog',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.blog.create-blog');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required'
        ]);
        
        $article = new Article;
        $article->title = $request->title;
        $article->body_md = $request->body;
        $article->summary_md = $request->summary;
        $article->online = $request->online;
        $article->slug = str_slug($article->title, '-') . '-' . $article->id;
        $article->body_html = GitDown::parseAndCache($request->body);
        $article->summary_html = GitDown::parseAndCache($request->summary);
        $article->save();
        return redirect()->route('blog-admin.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($param)
    {
        //seach by id or the slug
        $articleFound = Article::where('id', $param)
                        ->orWhere('slug', $param)
                        ->firstOrFail();
            if ($articleFound->online){
            //if article is published, go to article page
            return view('backend.blog.blog-details', ['article' => $articleFound]);
        }

        //if article not published, redirect to articles.index page
        return redirect()->route('blog-admin.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $articleFoundedit = Article::where('slug',$id)->firstOrFail();
        return view('backend.blog.edit-blog',['articleFound'=>$articleFoundedit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $articleFound = Article::where('slug',$id)->firstOrFail();
        $articleFound->title = $request->title;
        $articleFound->body_md = $request->body;
        $articleFound->summary_md = $request->summary;
        $articleFound->online = $request->online;
        $articleFound->slug = str_slug($articleFound->title, '-').'-'.$article->id;
        $articleFound->body_html = GitDown::parseAndCache($request->body);
        $articleFound->summary_html = GitDown::parseAndCache($request->summary);
        $articleFound->save();
        return redirect()->route('blog-admin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $articleFound = Article::find($id);
        $articleFound->delete();
        return redirect()->route('blog-admin.index');
    }
}
