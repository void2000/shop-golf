<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view('backend.login.login');
    }

    public function PostLogin(request $request)
    {
        
        //Check validate
        $request->validate([
            'username'=>'required|min:6',
            'password'=>'required|min:6|max:32'
        ],[
            'username.required'=>'User không được để trống',
            'username.min'=>'User phải có ít nhất  6 kí tự',
            'password.required'=>'Password không được để trống',
            'password.min'=>'Password phải có ít nhất 6 kí tự',
            'password.max'=>'Password có nhiều nhất 32 kí tự',
        ]);

        if(Auth::attempt(['name' => $request->username, 'password' => $request->password]))
        {
            return redirect('/Home-Admin-Shop');
        }
        else
        {
         return redirect('/login-admin-golf')->with('error','Đăng nhập thất bại!');
        }


    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login-admin-golf');
    }

}
