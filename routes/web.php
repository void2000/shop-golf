<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//********************************
//*           frontend           *
//********************************
Route::group(['prefix' => '/'], function() {
    Route::resource('/', 'frontend\HomePageController');
    Route::resource('/blog', 'frontend\BlogController');
    
});

//********************************
//*           backend            *
//********************************

// login
Route::get('/login-admin-golf', 'backend\LoginController@index')->middleware('CheckLogout');;

Route::post('/login-admin-golf', 'backend\LoginController@PostLogin');

Route::get('/logout-admin-golf', 'backend\LoginController@logout');
// end login

Route::group(['prefix' => '/Home-Admin-Shop', 'middleware' => ['CheckLogin']], function() {

    Route::get('/','backend\HomeAdminController@index')->name('dashboard');
    // blog
    Route::prefix('blog-admin')->name('blog-admin.')->group(function () {
        Route::resource('/', 'backend\ArticleController');
        Route::get('/{id}/edit', 'backend\ArticleController@edit')->name('blog-admin.edit');
        Route::post('/{id}/update', 'backend\ArticleController@update')->name('blog-admin.update');
        Route::get('/{id}/destroy', 'backend\ArticleController@destroy')->name('blog-admin.destroy');
    });
    //end route

});





