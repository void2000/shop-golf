<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['id'=> 1,'name'=>'binh1812k', 'email'=>'admin@gmail.com','password'=>bcrypt('admin123'),'active'=>1],

        ]);
    }
}
